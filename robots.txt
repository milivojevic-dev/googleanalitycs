User-agent: *

Disallow: /about.html/

Disallow: /products.html/

Disallow: /contact.html/

Sitemap: https://milivojevic-dev.gitlab.io/googleanalitycs/sitemap.xml